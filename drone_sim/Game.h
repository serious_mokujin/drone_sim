#pragma once
#include "BaseGame.h"

class DroneEntity;
class Gamepad;

class Game : public BaseGame
{
public:
	Game();
	virtual ~Game();

	void OnStart() override;
	void OnShutDown() override;

	void Update(float dt) override;
	void Render(float dt) override;


	void UpdateInput();

	void OnKeyRelease(irr::EKEY_CODE key) override;
	void OnKeyDown(irr::EKEY_CODE key) override;
private:
	void LoadLevel();

	void createGround();

	void createCube();

	irr::scene::ICameraSceneNode *m_camera;
	Gamepad* m_gamepad;
	std::shared_ptr<DroneEntity> m_drone;
};

