#pragma once

#include "irrlicht.h"
using namespace irr::core;
#define WIN32_LEAN_AND_MEAN // We don't want the extra stuff like MFC and such
#include <windows.h>
#include <XInput.h>     // XInput API
#pragma comment(lib, "XInput.lib")   // Library. If your compiler doesn't support this type of lib include change to the corresponding one

enum class GamePadButton
{
	DPAD_UP = 0,
	DPAD_DOWN = 1,
	DPAD_LEFT = 2,
	DPAD_RIGHT = 3,
	START = 4,
	BACK = 5,
	LEFT_THUMB = 6,
	RIGHT_THUMB = 7,
	LEFT_SHOULDER = 8,
	RIGHT_SHOULDER = 9,
	A = 10,
	B = 11,
	X = 12,
	Y = 13,
	Count = 14
};

// GamePad Indexes
enum class GamePadIndex
{
	One = 0,
	Two = 1,
	Three = 2,
	Four = 3,
};

// The GamePad State Stuct, were we store the buttons positions
struct GamePadState
{
	bool		_buttons[(size_t)GamePadButton::Count];
	vector2df 	_left_thumbstick;               
	vector2df	_right_thumbstick;
	float		_left_trigger;
	float		_right_trigger;
	// Just to clear all values to default
	void reset();
};

class Gamepad
{
public:
	Gamepad(GamePadIndex player);
	virtual ~Gamepad(void);

	bool is_connected();
	void vibrate(float leftmotor = 0.0f, float rightmotor = 0.0f);
	void update();
	bool Pressed(GamePadButton button);

public:
	GamePadState	State;
private:
	XINPUT_STATE _controllerState;
	GamePadIndex _playerIndex;
};

