#pragma once

namespace utils
{
	//template <T>
	//inline float lerp<T>(T from, T to, float t)
	//{
	//	return from + (to - from) * t;
	//}
#define lerp(from, to, t) (from) + ((to) - (from)) * (t)
}