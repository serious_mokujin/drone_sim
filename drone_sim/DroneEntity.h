#pragma once
#include "irrlicht.h"

using namespace irr::core;
using namespace irr::scene;

class irrBulletWorld;
class IRigidBody;
class EffectHandler;

class DroneEntity
{
public:
	//const static float k_engine_max_rpm = 3600.0f;
	const static int k_enginesCount = 4;
public:
	DroneEntity();
	~DroneEntity();

	vector3df GetPosition() const;

	void Load(irr::IrrlichtDevice* device, irrBulletWorld* world, EffectHandler* effect);
	void SetUp();
	void Update(float dt);
	void Unload(EffectHandler* effect);

	void SetAllEnginesPower(float power);
	void SetEnginePower(int engine, float power);

private:

	void UpdateVaneRotation(float dt);

private:

	bool m_loaded;

	irr::scene::ISceneNode* m_rootNode;
	IRigidBody *m_body;
	vector3df	m_upVector;

	IMeshSceneNode *m_frontLeftVane;
	IMeshSceneNode *m_frontRightVane;
	IMeshSceneNode *m_backLeftVane;
	IMeshSceneNode *m_backRightVane;
	float m_vaneInertia[k_enginesCount];

	float m_engineForces[k_enginesCount];
	vector3df m_engineForceApplyPoints[k_enginesCount];
};

