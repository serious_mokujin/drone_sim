#pragma once
#include "irrlicht.h"
#include "irrbullet.h"
#include "XEffects/XEffects.h"

class BaseGame : public irr::IEventReceiver
{
public:
	BaseGame();
	virtual ~BaseGame();

	
	void Run();
protected:
	virtual void OnStart();
	virtual void OnShutDown();
	virtual void Update(float dt);
	virtual void Render(float dt);

	virtual bool OnEvent(const irr::SEvent& event);

	virtual void OnKeyPress(irr::EKEY_CODE key) {}
	virtual void OnKeyDown(irr::EKEY_CODE key) {}
	virtual void OnKeyRelease(irr::EKEY_CODE key) {}

protected:
	irr::IrrlichtDevice* m_device;
	irr::video::IVideoDriver* m_driver;
	irr::scene::ISceneManager* m_scene;
	irr::gui::IGUIEnvironment* m_gui;
	irrBulletWorld* m_world;
	EffectHandler* m_effect;
	bool m_keyIsDown[irr::KEY_KEY_CODES_COUNT];

	bool m_debugEnabled;
};

