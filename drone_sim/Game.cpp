#include "stdafx.h"
#include "Game.h"
#include "DroneEntity.h"
#include "Gamepad.h"
#include "Utils.h"

using namespace irr;
using namespace irr::core;
using namespace irr::video;
using namespace irr::scene;

Game::Game()
{
	m_gamepad = new Gamepad(GamePadIndex::One);
}

Game::~Game()
{
	delete m_gamepad;
}

void Game::OnStart()
{
	BaseGame::OnStart();
	m_device->setWindowCaption(L"Drone sim");
	//m_gui->addStaticText(L"Hello World! This is the Irrlicht Software renderer!", rect<s32>(10, 10, 260, 22), true);

	m_camera = m_scene->addCameraSceneNode(0, vector3df(0, 5, -10), vector3df(0, 5, 0));
	/*
	m_camera = m_scene->addCameraSceneNodeFPS(nullptr, 100.0f, 0.008f);
	m_camera->setNearValue(0.01f);
	m_camera->setFarValue(10000.0f);
	m_camera->setPosition(vector3df(0, 5, -10));
	m_device->getCursorControl()->setVisible(false);
	*/

	// create light

	scene::ISceneNode* lightNode = m_scene->addLightSceneNode(0, core::vector3df(10, 30, 0), video::SColorf(1.0f, 0.6f, 0.7f, 1.0f), 25.0f);
	m_scene->setAmbientLight(video::SColorf(0.1f, 0.1f, 0.1f, 1.0f));
	//scene::ISceneNodeAnimator* anim = m_scene->createFlyCircleAnimator(core::vector3df(0, 150, 0), 250.0f);
	//lightNode->addAnimator(anim);
	//anim->drop();

	u32 shadowDimen = 2048;
	m_effect->addShadowLight(SShadowLight(shadowDimen, vector3df(10, 30, 0), vector3df(0, -1, 0),
		SColor(0, 132, 164, 128), 20.0f, 60.0f, 30.0f * DEGTORAD));

	m_scene->setShadowColor(video::SColor(150, 0, 0, 0));

	LoadLevel();
}

void Game::OnShutDown()
{
	BaseGame::OnShutDown();
}

void Game::Update(float dt)
{
	BaseGame::Update(dt);
	m_drone->Update(dt);
	if (m_gamepad->is_connected())
	{
		m_gamepad->update();
		UpdateInput();
	}

	m_camera->setTarget(m_drone->GetPosition());
	vector3df droneDirection = m_drone->GetPosition() - m_camera->getPosition();
	float droneDistance = droneDirection.getLength();
	droneDirection.normalize();
	vector3df cameraNewPosition = m_camera->getPosition();
	float newDistance = lerp(droneDistance, 3, dt * 5);
	cameraNewPosition += droneDirection * (droneDistance - newDistance);
	
	float camHeight = m_camera->getPosition().Y - m_drone->GetPosition().Y;
	float newHeight = lerp(camHeight, 2, dt * 10);
	cameraNewPosition.Y = m_drone->GetPosition().Y + newHeight;
	m_camera->setPosition(cameraNewPosition);

}

void Game::Render(float dt)
{
	BaseGame::Render(dt);
}

void Game::LoadLevel()
{
	//m_scene->getParameters()->setAttribute(COLLADA_CREATE_SCENE_INSTANCES, true);
	//m_scene->getMesh("../resources/scene.dae");

	createGround();

	createCube();

	m_drone.reset(new DroneEntity());
	m_drone->Load(m_device, m_world, m_effect);
	m_drone->SetUp();
}


// To add a rigid body with collision masks, simply add two extra parameters to the creation code:
// world->addRigidBody(shape, EMGM_GROUND, collideWithRigidBody);
// instead of world->addRigidBody(shape)
void Game::createGround()
{
	IMeshSceneNode *Node = m_device->getSceneManager()->addCubeSceneNode(1.0);
	Node->setScale(vector3df(600, 3, 600)); // 400, 3, 400
	Node->setPosition(vector3df(200, 0, 100));
	Node->getMaterial(0).Lighting = false;
	Node->setMaterialFlag(video::EMF_NORMALIZE_NORMALS, true);
	m_device->getSceneManager()->getMeshManipulator()->makePlanarTextureMapping(Node->getMesh(), 0.004f);
	m_effect->addShadowToNode(Node, E_FILTER_TYPE::EFT_16PCF, E_SHADOW_MODE::ESM_RECEIVE);

	//Node->setMaterialTexture(0, m_device->getVideoDriver()->getTexture("rockwall.jpg"));

	//if (drawWireFrame)
	//	Node->setMaterialFlag(EMF_WIREFRAME, true);

	ICollisionShape *shape = new IBoxShape(Node, 0, false);

	IRigidBody *body;
	body = m_world->addRigidBody(shape);
}

void Game::createCube()
{
	ISceneNode *Node = m_device->getSceneManager()->addCubeSceneNode(1.0);
	//Node->setScale(vector3df(600, 3, 600)); // 400, 3, 400
	Node->setPosition(core::vector3df(10, 30, 0));
	Node->getMaterial(0).Lighting = false;
	Node->setMaterialFlag(video::EMF_NORMALIZE_NORMALS, true);
	//Node->setMaterialTexture(0, m_device->getVideoDriver()->getTexture("rockwall.jpg"));

	//if (drawWireFrame)
	//	Node->setMaterialFlag(EMF_WIREFRAME, true);

	//ICollisionShape *shape = new IBoxShape(Node, 1, true);

	//IRigidBody *body;
	//body = m_world->addRigidBody(shape);

}
void Game::OnKeyRelease(irr::EKEY_CODE key)
{
	if (key == KEY_KEY_P)
	{
		m_world->pauseSimulation(!m_world->simulationPaused());
	}

	if (key == irr::KEY_F3)
	{
		ICameraSceneNode *camera = m_scene->getActiveCamera();
		bool enabled = camera->isInputReceiverEnabled();
		camera->setInputReceiverEnabled(!enabled);
		m_device->getCursorControl()->setVisible(enabled);
	}

	
	if (key == KEY_KEY_Q)
	{
		m_drone->SetEnginePower(0, 0.0f);
	}
	if (key == KEY_KEY_W)
	{
		m_drone->SetEnginePower(1, 0.0f);
	}
	if (key == KEY_KEY_A)
	{
		m_drone->SetEnginePower(2, 0.0f);
	}
	if (key == KEY_KEY_S)
	{
		m_drone->SetEnginePower(3, 0.0f);
	}

	if (key == KEY_KEY_R)
	{
		m_drone->Load(m_device, m_world, m_effect);
		m_drone->SetUp();
	}

	if (key == KEY_KEY_D)
	{
		m_debugEnabled = !m_debugEnabled;
	}
}

void Game::OnKeyDown(irr::EKEY_CODE key)
{
	float force = 0.6;
	if (key == KEY_KEY_Q)
	{
		m_drone->SetEnginePower(0, force);
	}
	if (key == KEY_KEY_W)
	{
		m_drone->SetEnginePower(1, force);
	}
	if (key == KEY_KEY_A)
	{
		m_drone->SetEnginePower(2, force);
	}
	if (key == KEY_KEY_S)
	{
		m_drone->SetEnginePower(3, force);
	}
}

void Game::UpdateInput()
{
	float leftSticValue = m_gamepad->State._left_thumbstick.Y;
	m_drone->SetAllEnginesPower(leftSticValue);
	if (m_gamepad->Pressed(GamePadButton::BACK))
	{
	}
}