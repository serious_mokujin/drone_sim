#include "stdafx.h"
#include "BaseGame.h"

#include <ctime>

using namespace irr;
using namespace irr::core;
using namespace irr::video;

BaseGame::BaseGame()
	: m_device(nullptr)
	, m_driver(nullptr)
	, m_scene(nullptr)
	, m_gui(nullptr)
	, m_debugEnabled(false)
{
}


BaseGame::~BaseGame()
{
}

void BaseGame::OnStart()
{
	m_device = createDevice(video::EDT_OPENGL, core::dimension2d<u32>(1600, 900), 32, false, true, false, this);
	m_driver = m_device->getVideoDriver();
	m_scene = m_device->getSceneManager();
	m_gui = m_device->getGUIEnvironment();

	m_world = createIrrBulletWorld(m_device, true, true);
	m_world->setDebugMode(EPDM_DrawAabb |	EPDM_DrawContactPoints);
	m_world->setGravity(vector3df(0, -9.8, 0));

	E_FILTER_TYPE filterType = E_FILTER_TYPE::EFT_16PCF;

	m_effect = new EffectHandler(m_device, m_driver->getScreenSize(), false, true);
	// Set a global ambient color. A very dark gray.
	m_effect->setAmbientColor(SColor(255, 32, 32, 32));
	// Set the background clear color to orange.
	m_effect->setClearColour(SColor(255, 250, 100, 0));

	// Add some post processing effects, a very subtle bloom here.
	const stringc shaderExt = (m_driver->getDriverType() == EDT_DIRECT3D9) ? ".hlsl" : ".glsl";

	m_effect->addPostProcessingEffectFromFile(core::stringc("shaders/BrightPass") + shaderExt);
	//m_effect->addPostProcessingEffectFromFile(core::stringc("shaders/BlurHP") + shaderExt);
	//m_effect->addPostProcessingEffectFromFile(core::stringc("shaders/BlurVP") + shaderExt);
	m_effect->addPostProcessingEffectFromFile(core::stringc("shaders/BloomP") + shaderExt);
}

void BaseGame::OnShutDown()
{
	if (m_device)
	{
		m_device->drop();
		m_device = nullptr;
		m_driver = nullptr;
		m_scene = nullptr;
		m_gui = nullptr;
	}
}

void BaseGame::Update(float dt)
{
	// Step the simulation with our delta time
	m_world->stepSimulation(dt, 120);

	for (irr::u32 i = 0; i < irr::KEY_KEY_CODES_COUNT; i++)
	{
		if (m_keyIsDown[i])
		{
			OnKeyDown((EKEY_CODE)i);
		}
	}
}
void BaseGame::Render(float dt)
{
	m_driver->beginScene(true, true, SColor(255, 100, 101, 140));

	//m_scene->drawAll();
	// EffectHandler->update() replaces smgr->drawAll(). It handles all
	// of the shadow maps, render targets switching, post processing, etc.
	m_effect->update();

	m_gui->drawAll();
	if (m_debugEnabled)
	{
		m_world->debugDrawWorld(true);
		// This call will draw the technical properties of the physics simulation
		// to the GUI environment.
		m_world->debugDrawProperties(true);
	}
	m_driver->endScene();
}
void BaseGame::Run()
{
	OnStart();

	u32 TimeStamp = m_device->getTimer()->getTime();
	u32 DeltaTime = 0;
	while (m_device->run())
	{
		DeltaTime = m_device->getTimer()->getTime() - TimeStamp;
		TimeStamp = m_device->getTimer()->getTime();
		float dt = (float)DeltaTime*0.001f;
		Update(dt);
		Render(dt);
	}

	OnShutDown();
}

bool BaseGame::OnEvent(const irr::SEvent& event)
{
	switch (event.EventType)
	{
	case EET_MOUSE_INPUT_EVENT:
	{
		if (event.MouseInput.Event == EMIE_RMOUSE_PRESSED_DOWN)
		{

		}
	}
	break;

	case EET_KEY_INPUT_EVENT:
	{
		if (event.KeyInput.PressedDown && !m_keyIsDown[event.KeyInput.Key])
		{
			OnKeyPress(event.KeyInput.Key);
		}
		else if (!event.KeyInput.PressedDown)
		{
			OnKeyRelease(event.KeyInput.Key);
		}
		m_keyIsDown[event.KeyInput.Key] = event.KeyInput.PressedDown;

		if (event.KeyInput.Key == KEY_KEY_P && event.KeyInput.PressedDown == false)
		{
			m_world->pauseSimulation(!m_world->simulationPaused());
		}
	}
	break;
	default:
		break;
	}
	return false;
}