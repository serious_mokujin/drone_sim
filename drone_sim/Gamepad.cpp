#include "stdafx.h"
#include "Gamepad.h"


Gamepad::Gamepad(GamePadIndex player)
{
	_playerIndex = player;
	State.reset();
}

Gamepad::~Gamepad(void)
{
	// We don't want the controller to be vibrating accidentally when we exit the app
	if (is_connected()) vibrate(0.0f, 0.0f);
}

bool Gamepad::is_connected()
{
	// clean the state
	memset(&_controllerState, 0, sizeof(XINPUT_STATE));

	// Get the state
	DWORD Result = XInputGetState((DWORD)_playerIndex, &_controllerState);

	if (Result == ERROR_SUCCESS)	return true;
	else return false;
}

void Gamepad::vibrate(float leftmotor, float rightmotor)
{
	// Create a new Vibraton 
	XINPUT_VIBRATION Vibration;

	memset(&Vibration, 0, sizeof(XINPUT_VIBRATION));

	int leftVib = (int)(leftmotor*65535.0f);
	int rightVib = (int)(rightmotor*65535.0f);

	// Set the Vibration Values
	Vibration.wLeftMotorSpeed = leftVib;
	Vibration.wRightMotorSpeed = rightVib;
	// Vibrate the controller
	XInputSetState((DWORD)_playerIndex, &Vibration);
}

void Gamepad::update()
{
	State.reset();
	// The values of the Left and Right Triggers go from 0 to 255. We just convert them to 0.0f=>1.0f
	if (_controllerState.Gamepad.bRightTrigger && _controllerState.Gamepad.bRightTrigger < XINPUT_GAMEPAD_TRIGGER_THRESHOLD)
	{
		State._right_trigger = _controllerState.Gamepad.bRightTrigger / 255.0f;
	}

	if (_controllerState.Gamepad.bLeftTrigger && _controllerState.Gamepad.bLeftTrigger < XINPUT_GAMEPAD_TRIGGER_THRESHOLD)
	{
		State._left_trigger = _controllerState.Gamepad.bLeftTrigger / 255.0f;
	}

	// Get the Buttons
	State._buttons[(int)GamePadButton::A] = (bool)(_controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_A);
	State._buttons[(int)GamePadButton::B] = (bool)_controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_B;
	State._buttons[(int)GamePadButton::X] = (bool)_controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_X;
	State._buttons[(int)GamePadButton::Y] = (bool)_controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_Y;
	State._buttons[(int)GamePadButton::DPAD_DOWN] = (bool)_controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN;
	State._buttons[(int)GamePadButton::DPAD_UP] = (bool)_controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP;
	State._buttons[(int)GamePadButton::DPAD_LEFT] = (bool)_controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT;
	State._buttons[(int)GamePadButton::DPAD_RIGHT] = (bool)_controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT;

	State._buttons[(int)GamePadButton::RIGHT_SHOULDER] = (bool)_controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER;
	State._buttons[(int)GamePadButton::LEFT_SHOULDER] = (bool)_controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER;
	State._buttons[(int)GamePadButton::RIGHT_THUMB] = (bool)_controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_THUMB;
	State._buttons[(int)GamePadButton::LEFT_THUMB] = (bool)_controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_THUMB;
	State._buttons[(int)GamePadButton::BACK] = (bool)_controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_BACK;
	State._buttons[(int)GamePadButton::RIGHT_SHOULDER] = (bool)_controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER;
	State._buttons[(int)GamePadButton::LEFT_SHOULDER] = (bool)_controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER;
	State._buttons[(int)GamePadButton::RIGHT_THUMB] = (bool)_controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_THUMB;
	State._buttons[(int)GamePadButton::LEFT_THUMB] = (bool)_controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_THUMB;
	State._buttons[(int)GamePadButton::BACK] = (bool)_controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_BACK;
	State._buttons[(int)GamePadButton::START] = (bool)_controllerState.Gamepad.wButtons & XINPUT_GAMEPAD_START;

	// Check to make sure we are not moving during the dead zone
	// Let's check the Left DeadZone
	if ((_controllerState.Gamepad.sThumbLX < XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE &&
		_controllerState.Gamepad.sThumbLX > -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE) &&
		(_controllerState.Gamepad.sThumbLY < XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE &&
			_controllerState.Gamepad.sThumbLY > -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE))
	{
		_controllerState.Gamepad.sThumbLX = 0;
		_controllerState.Gamepad.sThumbLY = 0;
	}

	// Check left thumbStick
	
	SHORT leftThumbY = _controllerState.Gamepad.sThumbLY;
	if (leftThumbY)
	{
		State._left_thumbstick.Y = ((float)leftThumbY / (float)0xFFFF) * 2.0f;
	}
	SHORT leftThumbX = _controllerState.Gamepad.sThumbLX;
	if (leftThumbX)
	{
		State._left_thumbstick.X = ((float)leftThumbX / (float)0xFFFF) * 2.0f;
	}

	// For the rightThumbstick it's pretty much the same.
}

bool Gamepad::Pressed(GamePadButton button)
{
	return State._buttons[(int)button];
}

void GamePadState::reset()
{
	for (int i = 0; i<(int)GamePadButton::Count; ++i) _buttons[i] = false;
	_left_thumbstick.set(0.0f, 0.0f);
	_right_thumbstick.set(0.0f, 0.0f);
	_left_trigger = _right_trigger = 0.0f;
}