#include "stdafx.h"
#include "DroneEntity.h"
#include "irrlicht.h"
#include "irrbullet.h"
#include "Utils.h"
#include "XEffects/XEffects.h"

using namespace irr;
using namespace irr::core;
using namespace irr::video;
using namespace irr::scene;

const float frame_diameter = 0.003f; // meters
const float frame_length = 0.4f; // meters
const float frame_height = 0.07f; // meters
const float frame_mass = 0.1f; // kg

DroneEntity::DroneEntity()
	: m_loaded(false)
	, m_rootNode(nullptr)
	, m_body(nullptr)
{
	memset(m_vaneInertia, 0, sizeof(float)*k_enginesCount);
	memset(m_engineForces, 0, sizeof(float)*k_enginesCount);
}


DroneEntity::~DroneEntity()
{
}

vector3df DroneEntity::GetPosition() const
{
	return m_rootNode->getPosition();
}

void DroneEntity::SetUp()
{
	m_body->translate(vector3df(0.0, 15, 0.0f));

	m_engineForceApplyPoints[0] = vector3df(-frame_length/2.0, 0.0, -frame_length / 2.0);
	m_engineForceApplyPoints[1] = vector3df(frame_length / 2.0, 0.0, frame_length / 2.0);
	m_engineForceApplyPoints[2] = vector3df(-frame_length / 2.0, 0.0, frame_length / 2.0);
	m_engineForceApplyPoints[3] = vector3df(frame_length / 2.0, 0.0, -frame_length / 2.0);
}

void DroneEntity::Update(float dt)
{
	m_upVector = vector3df(0.0, 1.0f, 0.0f);
	m_rootNode->getAbsoluteTransformation().rotateVect(m_upVector);
	m_upVector.normalize();

	for (int i = 0; i < k_enginesCount; i++)
	{
		vector3df applyPoint = m_engineForceApplyPoints[i];
		m_rootNode->getAbsoluteTransformation().rotateVect(applyPoint);
		m_body->applyForce(m_upVector * m_engineForces[i], applyPoint);
	}
	UpdateVaneRotation(dt);
	
}

void DroneEntity::Unload(EffectHandler* effect)
{
	
	effect->removeShadowFromNode(*m_rootNode->getChildren().begin());
	effect->removeShadowFromNode(m_frontLeftVane);
	effect->removeShadowFromNode(m_frontRightVane);
	effect->removeShadowFromNode(m_backLeftVane);
	effect->removeShadowFromNode(m_backRightVane);

	irrBulletWorld* world = m_body->getDynamicsWorld();
	world->removeCollisionObject(m_body, true);
	m_loaded = false;
}

void DroneEntity::Load(IrrlichtDevice* device, irrBulletWorld* world, EffectHandler* effect)
{
	if (m_loaded)
	{
		Unload(effect);
	}
	ISceneManager* scene = device->getSceneManager();

	m_rootNode = scene->addCubeSceneNode(frame_length);
	m_rootNode->setName("root");

	// hide cube by setting alpha to zero
	scene->getMeshManipulator()->setVertexColorAlpha(((IMeshSceneNode*)m_rootNode)->getMesh(), 0);

	m_rootNode->setScale(vector3df(1.0f, frame_height / frame_length, 1.0f));
	m_rootNode->setPosition(vector3df(0.0f, 0.0f, 0.0f));
	m_rootNode->setMaterialFlag(video::EMF_LIGHTING, false);
	m_rootNode->setMaterialFlag(video::EMF_NORMALIZE_NORMALS, true);
	ICollisionShape *frame1shape = new IBoxShape(m_rootNode, frame_mass, true);
	m_body = world->addRigidBody(frame1shape);
	m_body->setActivationState(EActivationState::EAS_DISABLE_DEACTIVATION);
	m_body->setDamping(0.1f, 0.1f);
	m_body->setFriction(1.0f);
	scene->getMeshManipulator()->scale(((IMeshSceneNode*)m_rootNode)->getMesh(), vector3df(0.01f));

	IMeshSceneNode* graphicsNode = scene->addMeshSceneNode(scene->getMesh("../resources/qudrocopter1_4_no_vanes.3ds"), m_rootNode);
	graphicsNode->setName("drone_mesh");
	vector3df mesh_scale = vector3df(1.0f, 1.0f, 1.0f) / m_rootNode->getScale();
	mesh_scale *= 0.06f;
	graphicsNode->setScale(mesh_scale);
	graphicsNode->setMaterialFlag(video::EMF_BACK_FACE_CULLING, true);
	graphicsNode->getMaterial(0).Lighting = false;
	graphicsNode->setMaterialFlag(video::EMF_NORMALIZE_NORMALS, true);

	effect->addShadowToNode(graphicsNode, E_FILTER_TYPE::EFT_16PCF);

	float vaneDist = 2.2;
	m_frontLeftVane = scene->addMeshSceneNode(scene->getMesh("../resources/front_w.3ds"), graphicsNode);
	m_frontLeftVane->setPosition(vector3df(-vaneDist, 0, vaneDist));
	effect->addShadowToNode(m_frontLeftVane, E_FILTER_TYPE::EFT_16PCF);

	m_frontRightVane = scene->addMeshSceneNode(scene->getMesh("../resources/front_w.3ds"), graphicsNode);
	m_frontRightVane->setPosition(vector3df(vaneDist, 0, vaneDist));
	effect->addShadowToNode(m_frontRightVane, E_FILTER_TYPE::EFT_16PCF);

	m_backLeftVane = scene->addMeshSceneNode(scene->getMesh("../resources/back_w.3ds"), graphicsNode);
	m_backLeftVane->setPosition(vector3df(-vaneDist, 0, -vaneDist));
	effect->addShadowToNode(m_backLeftVane, E_FILTER_TYPE::EFT_16PCF);

	m_backRightVane = scene->addMeshSceneNode(scene->getMesh("../resources/back_w.3ds"), graphicsNode);
	m_backRightVane->setPosition(vector3df(vaneDist, 0, -vaneDist));
	effect->addShadowToNode(m_backRightVane, E_FILTER_TYPE::EFT_16PCF);



	m_loaded = true;
}


void DroneEntity::SetAllEnginesPower(float power)
{
	m_engineForces[0] = power;
	m_engineForces[1] = power;
	m_engineForces[2] = power;
	m_engineForces[3] = power;
}

void DroneEntity::SetEnginePower(int engine, float power)
{
	m_engineForces[engine] = power;
}

void RotateVane(IMeshSceneNode *node, const float &engineValue, float &inertiaValue, float dt)
{
	float rotationScale = dt * 10000.0f;
	float inertiaSpeed = dt * 10.0f;
	vector3df prevRotation;
	float apt = 0.0;

	prevRotation = node->getRotation();
	inertiaValue = lerp(inertiaValue, engineValue, inertiaSpeed);
	apt = inertiaValue * rotationScale;
	node->setRotation(vector3df(prevRotation.X, prevRotation.Y + apt, prevRotation.Z));
}

void DroneEntity::UpdateVaneRotation(float dt)
{
	RotateVane(m_frontLeftVane, m_engineForces[2], m_vaneInertia[0], dt);
	RotateVane(m_frontRightVane, m_engineForces[1], m_vaneInertia[1], dt);
	RotateVane(m_backLeftVane, m_engineForces[0], m_vaneInertia[2], dt);
	RotateVane(m_backRightVane,  m_engineForces[3], m_vaneInertia[3], dt);
}